# parametricXY

A compact core XY implementation using linear rails, two lasercut/waterjet parts and ~8 printed parts.

<img src="parametricXY_CAD/parametricXY.png" width="600">
